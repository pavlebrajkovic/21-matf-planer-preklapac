const mongoose = require('mongoose');
const Note = require('../models/notesModel');
const User = require('../models/usersModel');

/**Za svakog korisnika cuvaju se samo njegove beleske
 * u telu zahteva treba da se nalaze polja: 
 * title (naslov beleske), description (telo beleske) i index (indeks studenta)
 */
module.exports.createNote = async (req, res, next) => {
    if (!req.body.title  || !req.body.index) {
      res.status(400);
      res.send();
    } else {
      try {
        const currentUser = await User.findOne({"indeks": req.body.index}).exec();
    
        if(currentUser){
          const newNote = new Note({
            _id: new mongoose.Types.ObjectId(),
            title: req.body.title,
            description: req.body.description,
            user: currentUser._id
          });
    
          await newNote.save();
    
          res.status(201).json(newNote);
        } else {
          res.status(400);
          res.send();
        }
        
      } catch (err) {
        next(err);
      }
    }
};

/**Za konkretnog usera treba da se dohvate njegove beleske
 * u parametrima zahteva treba da se navede indeks studenta
 */
module.exports.getNotes = async (req, res, next) => {
    try {

        const currentUser = await User.findOne({"indeks": req.query.index}).exec();
        
        if(currentUser){
            const notes = await Note.find({"user": currentUser._id}).exec();
            
            res.status(200).json(notes);
        } else {
            
            res.status(400);
            res.send();
        }
    } catch (err) {
      next(err);
    }    
};

/**
 * Beleska tekuceg korisnika sa odgovarajucim identifikatorom se modifikuje (menja se title ili description svojstvo)
 * potrebno je u telu zahteva proslediti index korisnika pod svojstvom index
 * i identifikator poruke pod svojstvom note_id
 */
module.exports.updateByNoteId = async (req, res, next) => {
    if (!req.body.note_id || !req.body.index) {
      console.log("id "+ req.body.note_id);
      console.log("index " + req.body.indeks);
      console.log("1.IF")
      res.status(400);
      res.send();
    } else {
      try {
          const currentUser = await User.findOne({"indeks": req.body.index});
          const currentNote = await Note.findOne({"_id": req.body.note_id, "user": currentUser._id});
          if(currentNote && currentUser){
            currentNote.title = req.body.title;
            currentNote.description = req.body.description;

            await currentNote.save();
    
            res.status(200).json(currentNote);      
        } else {
          console.log("2. IF")
          res.status(400);
          res.send();
        }
        
      } catch (err) {
        next(err);
      }
    }
};

/**
 * Brise belesku korisnika sa zadatim identifikacionim brojem;
 * Neophodno je navesti kao parametar zahteva identifikacioni broj beleske
 * pod svojstvom note_id
 */
module.exports.deleteByNoteId = async function (req, res, next) {
  try {
    await Note.deleteOne({ "_id": req.query.note_id }).exec();
    
    res.status(200).json({ message: 'The note is successfully deleted' });
  } catch (err) {
    next(err);
  }
};