
const mongoose = require('mongoose');

const notesSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  title: {
    type: String,
    required: true,
  },
  description: {
     type: String,
     require: false
  },
  user: {
      type: mongoose.Schema.Types.ObjectId,//tip je id jednog studenta
      ref: 'studenti',
      required: true
  }
});


module.exports = mongoose.model('notes', notesSchema);