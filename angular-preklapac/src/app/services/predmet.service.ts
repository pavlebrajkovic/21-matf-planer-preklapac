import { Injectable } from '@angular/core';
import { Predmet } from './../models/predmet.model';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { PredmetNiz } from '../models/predmetniz.model';
import { strict } from 'assert';
@Injectable({
  providedIn: 'root'
})
export class PredmetService {

  public indeks = "2002000";  // privremeno treba povezati sa indeksom na logovanju i ukinuti ga i na komponenti
  public predmeti: Observable<PredmetNiz>;

  private readonly rasporedurl = 'http://localhost:3004/students/raspored/'
  private readonly predmeturl = 'http://localhost:3004/students/predmet/'
  //private readonly rasporedfaksaurl = 'http://poincare.matf.bg.ac.rs/~kmiljan/raspored/sve/'
  constructor(private http: HttpClient) {

    this.getPredmeti(this.indeks);


  }


    // http://localhost:3000/students/raspored/:indeks
   public getPredmeti(indeks: string): Observable<PredmetNiz> {

     this.predmeti = this.http.get<PredmetNiz>(this.rasporedurl + indeks );
     //console.log(this.http.get(this.rasporedurl + "mr14245"));
     return this.predmeti;
   }
   public sacuvajRaspored(predmetii, indx)  {
    const body = { indeks: indx,
                  predmeti: predmetii }

    return this.http.post(this.rasporedurl, body);
   }

   public dodajPredmet (smer: string, ime: string):Observable<PredmetNiz>  {


    let params = new HttpParams().set('smer',smer).set('predmet', ime);

     return this.http.get<PredmetNiz>(this.predmeturl, {'params': params});
      
      







   }
}
