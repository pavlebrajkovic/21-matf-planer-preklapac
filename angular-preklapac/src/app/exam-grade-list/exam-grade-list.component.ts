import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {ExamGrade} from './shared/examGrade.model';
import {ExamGradeService} from './shared/exam-grade.service';

@Component({
  selector: 'app-exam-grade-list',
  templateUrl: './exam-grade-list.component.html',
  styleUrls: ['./exam-grade-list.component.css']
})
export class ExamGradeListComponent implements OnInit {
  public exams: Observable<ExamGrade[]>;
  public addExam: boolean;
  public updateExam: boolean;

  private subjectName: string;
  private testPointsStr: string;
  private examPointsStr: string;

  private updateTestPointsStr: string;
  private updateExamPointsStr: string;

  public currentExamId: string;

  /**Signal salje noteDetailsComponent kako bi se NoteList refreshovala */
  constructor(private examService: ExamGradeService) {
      if(localStorage.getItem('isLoggedIn') === "true"){
        this.exams = this.examService.getAllExams();
        this.exams = this.examService.refreshExams();
        this.addExam = false;      
    }
  }

  ngOnInit(): void {
    
  }

  addExamPoints(){
    let testPoints = Number.parseInt(this.testPointsStr);
    let examPoints = Number.parseInt(this.examPointsStr);
    this.examService.addExam(this.subjectName, testPoints, examPoints).subscribe((result) => {
      
      this.addExam = false;
      this.exams = this.examService.refreshExams();
      this.subjectName = "";
      this.examPointsStr = "";
      this.testPointsStr = "";
    },
    (err) => {
      console.log("Nije uspelo");
    });
  }

  deleteExam(exam_id: string){
    this.examService.deleteExam(exam_id).subscribe((res)=>{
      this.exams = this.examService.refreshExams();
    }, (err) => {
      console.log("Brisanje nije uspelo!");
    });
  }

  updateExamPoints(){
    let testPoints = Number.parseInt(this.updateTestPointsStr);
    let examPoints = Number.parseInt(this.updateExamPointsStr);
    
    this.examService.updateExam(this.currentExamId, testPoints, examPoints).subscribe((res) => {
      this.exams = this.examService.refreshExams();

      this.updateExamPointsStr = "";
      this.updateTestPointsStr = "";
      this.currentExamId = "";
    }, (err) =>{
      console.log("Nije uspeo!");
    });
  }

}


