export class ExamGrade{
    _id: string;
    subject: string;
    testPoints: number;
    examPoints: number;
    grade: number;
  }
  