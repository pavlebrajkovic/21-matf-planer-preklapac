import { TestBed } from '@angular/core/testing';

import { RefreshSignalService } from './refresh-signal.service';

describe('RefreshSignalService', () => {
  let service: RefreshSignalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RefreshSignalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
