import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Note } from '../../shared/note.model';
import { NotesService } from '../../shared/notes.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {RefreshSignalService} from '../../shared/refresh-signal.service';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.css']
})
export class NoteDetailsComponent implements OnInit {

  public note: Note = new Note();
  //id tekuce beleske
  noteId: number;
  //da li se pravi nova beleska
  new: boolean;


  constructor(private notesService: NotesService,
    private signalService: RefreshSignalService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    //treba da proverimo da li se menja postojeca beleska ili se pravi nova

    //callback fja koja ce se izvrsiti svaki put kad se promene parametri
    let sub = this.route.params.subscribe((params: Params) => {
      if(params.id){
        this.notesService.subsArray.push(this.notesService.getAll().subscribe((notes: Note[])=>{
          this.note = notes[params.id];
        }));

        this.noteId = params.id;
        this.new = false;
      } else {
        //this.note = new Note();
        this.new = true;
      }
    });
    sub.unsubscribe();
  }

  onSubmit(form: NgForm){
    if(this.new){
      //treba da sacuvamo belesku
      this.notesService.add(form.value).subscribe((note: Note) =>{
        this.sendSignal();
      });
      
    } else {
      //treba da azuriramo postojecu belesku
      let wantedNoteId;
      let t = form.value.title;

      let d = form.value.description;

      this.notesService.subsArray.push(this.notesService.getAll().subscribe((notes: Note[])=>{
        wantedNoteId = notes[this.noteId]._id;

        this.notesService.subsArray.push(this.notesService.update(wantedNoteId, t, d).subscribe((note: Note)=>{
          this.sendSignal();
        }));
        
      }));
      
    }
    this.router.navigate(['/home-page'], { fragment: 'note'});
    //this.router.navigateByUrl('/home-page');
    
  }

  cancel(){
    this.router.navigate(['/home-page'], { fragment: 'note' });
    //this.router.navigateByUrl('/home-page');

  }

  /**Metod za slanje signala preko RefreshSignalServica za osvezavanje liste beleski 
   * signal ce biti 1 ako ima potrebe za osvezavanjem liste
  */
  sendSignal(): void{
    this.signalService.sendSignal(1);
  }

}
