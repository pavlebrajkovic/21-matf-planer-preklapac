import {Predmet} from './predmet.model';


export interface PredmetNiz {
    _id : string;
    indeks: string
    predmeti: Predmet[];
}